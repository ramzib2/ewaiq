// import { getRandom } from "@/utils/functions/varities";
// import { deleteCookie, getCookie, setCookie } from "cookies-next";
export enum CookieKeys {
  access_token = "access_token",
  refresh_token = "refresh_token",
  language = "NEXT_LOCALE", //Don't change it, nextjs specifications
  branch_id = "branch_id",
  agent_id = "agent_id",
}
const storeCookei = (key: string, value: string, age: number = 30) => {
  // setCookie(key, value, {
  //   path: "/",
  //   maxAge: 60 * 60 * 24 * age,
  // });
};
//============== branch id ================
export const storeBranchIdCookie = (branchId: number) => {
  storeCookei(CookieKeys.branch_id, branchId.toString());
};
export const getBranchIdCookie = () => null;
// (getCookie(CookieKeys.branch_id) as string) ?? null;

//============== language ================
export const storeLangCookie = (lang: string) => {
  storeCookei(CookieKeys.language, lang);
};
export const getLangCookie = () => ""; //getCookie(CookieKeys.language) as string;
//============== Tokens ================
export const storeAccessToken = (accessToken: string) => {
  storeCookei(CookieKeys.access_token, accessToken, 30);
};
export const storeAuthTokens = (accessToken: string, refreshToken: string) => {
  storeCookei(CookieKeys.access_token, accessToken, 30);
  storeCookei(CookieKeys.refresh_token, refreshToken, 30);
};
export const deleteTokens = () => {
  // deleteCookie(CookieKeys.access_token);
  // deleteCookie(CookieKeys.refresh_token);
};
export const getAccessToken = () => ""; // getCookie(CookieKeys.access_token);
export const getRefreshToken = () => ""; // getCookie(CookieKeys.refresh_token);
//====================================
