import ApiProvider from "../baseApi/api-provider";
import RequestConfig from "../baseApi/request-config";
import {
  deleteTokens,
  getAccessToken,
  getRefreshToken,
  storeAuthTokens,
} from "./cookies";

export default class BaseClass extends ApiProvider {
  constructor(config: RequestConfig) {
    super({ ...config });
    this.instance.interceptors.request.use(
      (config) => {
        const token = getAccessToken();
        if (token) {
          config.headers["Authorization"] = "Bearer " + token;
        }
        return config;
      },
      (error) => Promise.reject(error)
    );
    this.instance.interceptors.response.use(
      (res) => res,
      async (err) => {
        const originalConfig = err.config;
        if (err.response) {
          if (err.response?.status === 401 && !originalConfig._retry) {
            const refreshToken = getRefreshToken();
            if (!refreshToken) return Promise.reject(err);
            originalConfig._retry = true;
            try {
              const rs = await this.instance.post("/users/refresh-token", {
                refresh_token: refreshToken,
              });
              const { access_token, refresh_token } = rs.data.result;
              storeAuthTokens(access_token, refresh_token);
              return this.instance(originalConfig);
            } catch (_error: any) {
              if (
                _error.response?.status === 422 ||
                _error.response?.status === 405
              )
                deleteTokens();
              return Promise.reject(_error);
            }
          }
        }
        return Promise.reject(err);
      }
    );
  }
}
