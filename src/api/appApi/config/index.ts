import {
  // getAgentIdCookie,
  // getBranchIdCookie,
  getLangCookie,
} from "@/api/base-service/cookies";
// import { isDev } from "@/utils/functions/global-constants";

export interface Config {
  baseURL: string;
  headers: {
    "Content-Type": string;
    Accept: string;
    "Accept-Language": string;
    Version: string;
    Platform: string;
    "Company-Id"?: string;
    // Authorization: string;
    referer?: string;
  };
}
export const baseURL = "https://ewaiq.com/public/api/v1";
// process.env.NEXT_PUBLIC_V3_BASE_URL as string;
// "https://ososs.com/api/micro-store/v3";
export const companyId = process.env.NEXT_PUBLIC_V3_COMPANY_ID as string;
// "2416";
// const version = process.env.NEXT_PUBLIC_V3_VERSION;
export const apiConfig = (
  lang?: string,
  host?: string,
  token?: string
): // language: string, host: string | null = null
Config => {
  let headers: any = {
    "Content-Type": "application/json",
    Accept: "application/json",
    "Accept-Language": lang ?? getLangCookie(),
    Version: "1.0",
    Platform: "web",
    // "Branch-Id": getBranchIdCookie() ?? null,
    Authorization: token ? `Bearer ${token}` : null,
  };
  // if (isDev)
  if (host) headers["referer"] = host; //must bereq.headers.host in ssr
  return {
    baseURL: baseURL,
    headers: headers,
  };
};
