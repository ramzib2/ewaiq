import ApiService from "@/api/baseApi/api-service";
import { Config } from "../../config";
import { IGetProduct, IGetProducts } from "./interfaces/product-interfaces";
const URLs = {
  allProducts: "/products/all",
  relatedProducts: "/products/related",
  getProduct: "/products/getProduct",
};
class ApiProducts extends ApiService {
  constructor(config: Config) {
    super({ ...config });
  }
  getProducts = (): Promise<IGetProducts> =>
    this.post(URLs.allProducts, { "per-page": 12, page: 1 });
  getRelatedProducts = (product_id: number): Promise<IGetProducts> =>
    this.post(URLs.relatedProducts, { product_id: product_id, limit: 7 });
  getProduct = (product_id: number): Promise<IGetProduct> =>
    this.post(URLs.getProduct, { product_id: product_id });
}
export default ApiProducts;
