import { useQuery } from "@tanstack/react-query";
import { apiConfig } from "../../config";
import ApiProducts from "./products-api";

export const useProducts = () => new ApiProducts(apiConfig());

export const useGetProducts = () => {
  const product = useProducts();
  return useQuery({
    queryKey: ["products"],
    queryFn: () => product.getProducts(),
    refetchOnWindowFocus: true,
    enabled: true,
  });
};
export const useGetProduct = (product_id: number) => {
  const product = useProducts();
  return useQuery({
    queryKey: ["product", product_id],
    queryFn: () => product.getProduct(product_id),
    refetchOnWindowFocus: true,
  });
};

export const useGetRelatedProducts = (product_id: number) => {
  const product = useProducts();
  return useQuery({
    queryKey: ["related-products", product_id],
    queryFn: () => product.getRelatedProducts(product_id),
    refetchOnWindowFocus: true,
  });
};
