export interface IProduct {
  allow_checkout_when_out_of_stock: number;
  approved_by: number;
  barcode: string;
  brand: {
    created_at: string;
    description: null;
    id: number;
    is_featured: number;
    logo: string;
    name: string;
    order: number;
    status: { value: "published"; label: "Published" };
    updated_at: Date;
    website: null;
  };
  brand_id: number;
  categories: {
    created_at: Date;
    description: string | null;
    icon: string;
    icon_image: null;
    id: number;
    image: string;
    is_featured: number;
    name: string;
    order: number;
    parent_id: number;
    pivot: {
      product_id: number;
      category_id: number;
    };
    status: { value: "published"; label: "Published" };
    updated_at: Date;
  }[];
  cbm: null;
  content: string;
  cost_per_item: null;
  created_at: Date;
  created_by_id: 9;
  created_by_type: string;
  default_variation: {
    id: number;
    product_id: number;
    configurable_product_id: number;
    is_default: number;
  };
  description: string;
  end_date: null;
  final_price: number;
  front_sale_price: number;
  generate_license_code: number;
  have_sample: false;
  height: null;
  id: number;
  image: string;
  images: string[];
  is_featured: number;
  is_in_wishlist: number;
  is_variation: number;
  length: null;
  name: string;
  options: [];
  order: number;
  order_request: number;
  original_price: number;
  per_unit: string;
  price: number;
  price_range: null;
  product_collections: [];
  product_labels: [];
  product_type: { value: "physical"; label: "Physical" };
  quantity: null;
  reviews: [];
  reviews_avg: null;
  reviews_count: number;
  sale_price: null;
  sale_type: number;
  sample_price: null;
  sku: string;
  slugable: {
    id: number;
    key: string;
    prefix: string;
    reference_id: number;
    reference_type: string;
  };
  start_date: null;
  status: { value: "published"; label: "Published" };
  stock_status: { value: "in_stock"; label: "In stock" };
  store: {
    address: string;
    city: null;
    company: null;
    content: string;
    country: null;
    created_at: Date;
    customer_id: number;
    description: string;
    email: null;
    id: number;
    logo: null;
    name: string;
    phone: null;
  };
  store_id: number;
  tax_id: null;
  taxes: {
    created_at: Date;
    id: number;
    percentage: number;
    pivot: {
      product_id: number;
      tax_id: number;
    };
    priority: 1;
    rules: [];
    status: { value: "published"; label: "Published" };
    title: string;
    updated_at: Date;
  }[];
  total_taxes_percentage: number;
  updated_at: Date;
  variations: any[]; //...to be typed
  views: number;
  weight: null;
  wide: null;
  with_storehouse_management: number;
}
export interface IGetProducts {
  code: number;
  message: string;
  response: string;
  data: {
    current_page: number;
    data: IProduct[];
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    links: string[];
    next_page_url: string;
    path: string;
    per_page: number;
    prev_page_url: number | null;
    to: number;
    total: number;
  };
}
export interface IGetProduct {
  code: number;
  message: string;
  response: string;
  data: IProduct;
}
