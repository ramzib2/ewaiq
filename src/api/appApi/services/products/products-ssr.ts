import { apiConfig } from "../../config";
import ApiProducts from "./products-api";

const productApi = new ApiProducts(apiConfig());
export const getProductsSSR = productApi.getProducts;
export const getProductSSR = productApi.getProduct;
export const getRelatedProductsSSR = productApi.getRelatedProducts;
