import { getProductSSR } from "@/api/appApi/services/products/products-ssr";
import Loading from "@/app/components/common/Loading";
import ProductDetails from "@/app/components/product-page/ProductDetails";
import { Suspense } from "react";

export default async function Page({ params }: { params: { id: number } }) {
  // await new Promise((resolve) => setTimeout(resolve, 3000));
  const productRaw = await getProductSSR(params.id);
  return (
    <Suspense fallback={<Loading />}>
      <ProductDetails product={productRaw.data} />
    </Suspense>
  );
}
