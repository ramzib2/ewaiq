import { Suspense } from "react";
import Advertisements from "./components/home-page/advertisements/Advertisements";
import Banner from "./components/home-page/banner/Banner";
import Blogs from "./components/home-page/blogs/Blogs";
import Brands from "./components/home-page/brands/Brands";
import Collection from "./components/home-page/collection/Collection";
import Offers from "./components/home-page/offers/Offers";
import OffersSC from "./components/home-page/offers/OffersSC";
import Loading from "./components/common/Loading";

export default function Home() {
  return (
    <div className="home-page">
      <Banner />
      <Advertisements />
      <Collection />
      <Suspense fallback={<Loading />}>
        <OffersSC />
      </Suspense>

      <Blogs />
      <Brands />
    </div>
  );
}
