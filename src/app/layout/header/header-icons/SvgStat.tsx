const SvgStat = () => {
  return (
    <svg
      style={{ cursor: "pointer" }}
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_1_24)">
        <path
          d="M37.998 6H9.99805C7.78805 6 5.99805 7.79 5.99805 10V38C5.99805 40.21 7.78805 42 9.99805 42H37.998C40.208 42 41.998 40.21 41.998 38V10C41.998 7.79 40.208 6 37.998 6ZM37.998 38H9.99805V10H37.998V38ZM17.998 34H13.998V24H17.998V34ZM25.998 34H21.998V14H25.998V34ZM33.998 34H29.998V20H33.998V34Z"
          fill="#231F20"
        />
      </g>
      <defs>
        <clipPath id="clip0_1_24">
          <rect width="48" height="48" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default SvgStat;
