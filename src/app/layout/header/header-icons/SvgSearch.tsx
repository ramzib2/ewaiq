const SvgSearch = () => {
  return (
    <svg
      style={{ cursor: "pointer" }}
      width="36"
      height="36"
      viewBox="0 0 36 36"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M23.6932 23.7167L31.5 31.5M27 15.75C27 21.9631 21.9631 27 15.75 27C9.53679 27 4.5 21.9631 4.5 15.75C4.5 9.53679 9.53679 4.5 15.75 4.5C21.9631 4.5 27 9.53679 27 15.75Z"
        stroke="#231F20"
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default SvgSearch;
