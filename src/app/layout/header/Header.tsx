"use client";

import Image from "next/image";
import Link from "next/link";
import SvgSearch from "./header-icons/SvgSearch";
import SvgStat from "./header-icons/SvgStat";
import SvgPerson from "./header-icons/SvgPerson";
import { NavLink } from "react-bootstrap";
import { useWindowDimension } from "@/hooks/useWinDim";

const Header = () => {
  useWindowDimension();
  return (
    <>
      <div style={{ marginBottom: 160 }} />
      <nav
        className="pc-header nav-bar w-100"
        style={{ position: "fixed", top: 0 }}
      >
        <Link href="/">
          <Image
            src="/images/Logo EWA-02 2.png"
            alt="RS"
            height={160}
            width={160}
            priority
          />
        </Link>
        <div className="main-menu">
          <Link href={"/"} passHref>
            <NavLink as="span">HOME</NavLink>
          </Link>
          <Link href={"/"} passHref>
            <NavLink as="span">PRODUCTS</NavLink>
          </Link>
          <Link href={"/"} passHref>
            <NavLink as="span">CART</NavLink>
          </Link>
          <Link href={"/"} passHref>
            <NavLink as="span">FAVORITE</NavLink>
          </Link>
        </div>
        <div style={{ marginInlineStart: "auto" }} />
        <div className="menu-icons">
          <SvgSearch />
          <SvgStat />
          <SvgPerson />
          <button className="btn-getapp">Get App</button>
        </div>
      </nav>
    </>
  );
};

export default Header;
