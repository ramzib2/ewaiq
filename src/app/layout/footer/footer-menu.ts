export interface IFooterMenu {
  title: string;
  items: {
    caption: string;
    href: string;
  }[];
}
export const menu1: IFooterMenu = {
  title: "Company",
  items: [
    { caption: "About us", href: "/" },
    { caption: "Affiliate", href: "/" },
    { caption: "Career", href: "/" },
    { caption: "Contact us", href: "/" },
  ],
};
export const menu2: IFooterMenu = {
  title: "Quick Links",
  items: [
    { caption: "Terms Of Use", href: "/" },
    { caption: "Terms & Conditions", href: "/" },
    { caption: "Refund Policy", href: "/" },
    { caption: "FAQs", href: "/" },
    { caption: "Privacy Policy", href: "/" },
  ],
};
export const menu3: IFooterMenu = {
  title: "Business",
  items: [
    { caption: "Our blog", href: "/" },
    { caption: "Cart", href: "/" },
    { caption: "My account", href: "/" },
    { caption: "Shop", href: "/" },
  ],
};
