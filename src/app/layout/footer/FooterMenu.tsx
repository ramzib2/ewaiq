import { IFooterMenu } from "./footer-menu";
interface Props {
  menu: IFooterMenu;
}
const FooterMenu: React.FC<Props> = ({ menu }) => {
  return (
    <div className="footer-menu">
      <div className="title">{menu.title}</div>
      {menu.items.map((item, key) => (
        <div key={key} className="item">
          {item.caption}
        </div>
      ))}
    </div>
  );
};

export default FooterMenu;
