import Image from "next/image";
import SvgCopywrite from "./footer-icons/SvgCopywrite";
import SvgYouTube from "./footer-icons/SvgYouTube";
import SvgTwitter from "./footer-icons/SvgTwitter";
import SvgFacebook from "./footer-icons/SvgFacebook";
import SvgInsta from "./footer-icons/SvgInsta";

const FooterSocial = () => {
  return (
    <div className="footer-social">
      <div className="footer-copyright">
        <Image
          src="/images/Logo EWA-02 2.png"
          alt="RS"
          height={250}
          width={250}
          priority
          style={{ opacity: 0.5 }}
        />
        <div>
          Copyright <SvgCopywrite /> {" 2024. All rights reserved."}
        </div>
      </div>
      <div style={{ marginInlineStart: "auto" }} />
      <div className="social-icons">
        <SvgYouTube />
        <SvgTwitter />
        <SvgFacebook />
        <SvgInsta />
      </div>
    </div>
  );
};

export default FooterSocial;
