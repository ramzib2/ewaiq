import FooterMenu from "./FooterMenu";
import FooterSocial from "./FooterSocial";
import { menu1, menu2, menu3 } from "./footer-menu";

const Footer = () => {
  return (
    <div className="footer">
      <div className="subscribe">
        <input className="subscribe-input" placeholder="Let’s stay in touch" />
        <button className="subscribe-btn">Subscribe</button>
      </div>
      <div className="row">
        <div className="col d-flex justify-content-center">
          <FooterMenu menu={menu1} />
        </div>
        <div className="col d-flex justify-content-center">
          <FooterMenu menu={menu2} />
        </div>
        <div className="col d-flex justify-content-center">
          <FooterMenu menu={menu3} />
        </div>
      </div>
      <div className="hor">Hi</div>
      <FooterSocial />
    </div>
  );
};

export default Footer;
