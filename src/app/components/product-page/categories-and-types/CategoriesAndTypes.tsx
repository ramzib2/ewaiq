import CategoriesTable from "./CategoriesTable";
import TypesOptions from "./types-options/TypesOptions";

const CategoriesAndTypes = () => {
  return (
    <div className="row d-flex catgories-and-types">
      <div className="col p-0">
        <CategoriesTable />
      </div>
      <div className="col p-0 col-auto">
        <TypesOptions />
      </div>
    </div>
  );
};

export default CategoriesAndTypes;
