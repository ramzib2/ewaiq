interface Props {
  selected: boolean;
}
const SvgRadio: React.FC<Props> = ({ selected }) => {
  return (
    <svg
      style={{ cursor: "pointer" }}
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
    >
      <circle cx="16" cy="16" r="14.5" stroke="#C0966B" strokeWidth="3" />
      {selected && <circle cx="16" cy="16" r="8" fill="#C0966B" />}
    </svg>
  );
};

export default SvgRadio;
