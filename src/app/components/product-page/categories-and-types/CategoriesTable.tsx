import SvgOneInfo from "../product-details-icons/SvgOneInfo";

const data = [
  {
    name: "TYPE:",
    value: "Bicycle Traffic/Light/Medium/Heavy",
  },
  {
    name: "Load:",
    value: "15kn/125kn/250kn/400kn",
  },
  {
    name: "Class:",
    value: "A15/B125/C250/D400",
  },
  {
    name: "Description:",
    value: [
      "It is lighter than metal.",
      "It does not require maintenance and there is no risk of theft.",
      "It is resistant to corrosion and does not rust.",
    ],
  },
];
const CategoriesTable = () => {
  return (
    <div className="categories-table-wrapper">
      <table>
        <tbody>
          <tr>
            <td colSpan={2}>Categories:</td>
          </tr>
          {data.map((item, key) => (
            <tr key={key}>
              <td>{item.name}</td>
              <td>
                {Array.isArray(item.value) ? (
                  <div className="array-td">
                    {(item.value as string[]).map((item, key) => (
                      <div key={key} className="item">
                        <div>
                          <SvgOneInfo />
                        </div>
                        {item}
                      </div>
                    ))}
                  </div>
                ) : (
                  item.value
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default CategoriesTable;
