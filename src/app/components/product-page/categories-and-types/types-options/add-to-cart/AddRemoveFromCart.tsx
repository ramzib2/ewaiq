import { useState } from "react";
import SvgIncDec from "./SvgIncDec";

const AddRemoveFromCart = () => {
  const [count, setCount] = useState(1);
  return (
    <div className="add-to-cart">
      <div className="add-remove">
        <SvgIncDec
          dec
          disabled={count <= 1}
          onClick={() => setCount(count - 1)}
        />
        {count}
        <SvgIncDec onClick={() => setCount(count + 1)} />
      </div>
      <button className="add-to-cart-btn">Add to cart +</button>
    </div>
  );
};

export default AddRemoveFromCart;
