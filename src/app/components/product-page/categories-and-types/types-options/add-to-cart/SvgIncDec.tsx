interface Props {
  dec?: boolean;
  disabled?: boolean;
  onClick: () => void;
}
const SvgIncDec: React.FC<Props> = ({ dec, disabled, onClick }) => {
  return (
    <svg
      style={{
        transform: dec ? "scaleX(-1)" : "",
        cursor: disabled ? "not-allowed" : "pointer",
      }}
      onClick={disabled ? undefined : onClick}
      xmlns="http://www.w3.org/2000/svg"
      width="51"
      height="56"
      viewBox="0 0 51 56"
      fill="none"
    >
      <path
        d="M45.6 19.6862C52 23.3812 52 32.6188 45.6 36.3138L14.4 54.3272C8 58.0222 0 53.4034 0 46.0133V9.98668C0 2.59659 8 -2.02222 14.4 1.67283L45.6 19.6862Z"
        fill={disabled ? "#d9d9d9" : "#C0966B"}
      />
      <text
        fill={"white"}
        fontSize={30}
        x="22"
        y={dec ? "28" : "30"}
        textAnchor="middle"
        alignmentBaseline="middle"
      >
        {dec ? "-" : "+"}
      </text>
    </svg>
  );
};

export default SvgIncDec;
