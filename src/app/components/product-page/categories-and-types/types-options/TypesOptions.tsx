import { useState } from "react";
import ParagraphTitle from "../../../common/ParagraphTitle";
import SvgRadio from "../SvgRadio";
import AddRemoveFromCart from "./add-to-cart/AddRemoveFromCart";
const radios = [
  { text: "Bicycle Traffic.", price: null },
  { text: "Light Duty.", price: "+8.00$" },
  { text: "Medium Duty.", price: "+10.00$" },
  { text: "Heavy Duty.", price: "+15.00$" },
];
const TypesOptions = () => {
  const [selected, setSelected] = useState(0);
  return (
    <div className="types">
      <ParagraphTitle title="TYPE" noMargin />
      <div className="options">
        {radios.map((item, index) => (
          <div
            key={index}
            className="option"
            onClick={() => setSelected(index)}
          >
            <div className="text">
              <div>
                <SvgRadio selected={index === selected} />
              </div>
              {item.text}
            </div>
            <div className="price">{item.price}</div>
          </div>
        ))}
      </div>
      <div className="input-field">
        <div className="label">Please enter your special requirements:</div>
        <input className="input-box" />
      </div>
      <AddRemoveFromCart />
    </div>
  );
};

export default TypesOptions;
