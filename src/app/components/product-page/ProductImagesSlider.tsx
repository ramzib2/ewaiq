import Image from "next/image";
import ThumbMailSlider from "../common/keen-slider/slider-with-thumbmails/ThumbMailSlider";
interface Props {
  productImages: string[];
  id: number;
}
const ProductImagesSlider: React.FC<Props> = ({ productImages, id }) => {
  return (
    <>
      {/* <div ref={containerRef} className="w-100"> */}
      <div className="w-100">
        <ThumbMailSlider
          thumbnails={productImages}
          id={id}
          sliderWidth={700}
          thumbPerview={700 / 96}
          size={96}
          isSamll={false}
        >
          {productImages?.map((img, key) => (
            <div
              key={key}
              style={{
                borderRadius: 16,
                display: "block",
                margin: "auto",
                textAlign: "center",
                overflow: "hidden",
              }}
            >
              <Image
                priority
                alt="img"
                src={img}
                width={700}
                height={700}
                // layout="fill"
                // className="custom-img"
                // objectFit="contain"
              />
            </div>
          ))}
        </ThumbMailSlider>
      </div>
    </>
  );
};

export default ProductImagesSlider;
