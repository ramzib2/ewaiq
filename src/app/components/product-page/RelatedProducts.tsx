import { useGetRelatedProducts } from "@/api/appApi/services/products";
import ParagraphTitle from "../common/ParagraphTitle";
import SvgNextPrev from "../home-page/offers/SvgNextPrev";
import { useState } from "react";
import Product from "../common/product/Product";
import Loading from "../common/Loading";

interface Props {
  id: number;
}
const step = 6;
const RelatedProducts: React.FC<Props> = ({ id }) => {
  const [start, setStart] = useState(0);
  const getRelatedProducts = useGetRelatedProducts(id);
  if (getRelatedProducts.isLoading) return <Loading />;
  const products = getRelatedProducts.data?.data?.data ?? [];
  const total = products?.length ?? 0;

  return (
    <div>
      <div className="offers-header">
        <ParagraphTitle title="Related Products" noMargin />
        <div className="offers-btns">
          <div className="all-btn">ALL</div>
          <SvgNextPrev
            prev
            disabled={start === 0}
            onClick={() => setStart(start - step)}
          />
          <SvgNextPrev
            disabled={start >= total - step}
            onClick={() => setStart(start + step)}
          />
        </div>
      </div>
      <div className="row m-0" style={{ columnGap: 80, rowGap: 48 }}>
        {products?.slice(start, start + step).map((product, key) => (
          <div key={key} className="col p-0">
            <Product product={product} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default RelatedProducts;
