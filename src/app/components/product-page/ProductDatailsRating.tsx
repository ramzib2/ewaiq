import SvgStars from "./product-details-icons/SvgStars";

interface Props {
  rate: number;
}
const ProductDatailsRating: React.FC<Props> = ({ rate }) => {
  return (
    <div>
      {Array(5)
        .fill(0)
        .map((_, index) => (
          <SvgStars key={index} full={rate > index} />
        ))}
    </div>
  );
};

export default ProductDatailsRating;
