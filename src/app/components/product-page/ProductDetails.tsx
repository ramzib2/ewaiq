"use client";
import { useGetProduct } from "@/api/appApi/services/products";
import ProductDatailsRating from "./ProductDatailsRating";
import CategoriesAndTypes from "./categories-and-types/CategoriesAndTypes";
import SvgLove from "./product-details-icons/SvgLove";
import SvgOneInfo from "./product-details-icons/SvgOneInfo";
import SvgStats from "./product-details-icons/SvgStats";
import Reviews from "./reviews/Reviews";
import Services from "./services/Services";
import RelatedProducts from "./RelatedProducts";
import ProductImagesSlider from "./ProductImagesSlider";
import { ImagBaseUrl } from "@/app/utils/common-constants";
import Loading from "../common/Loading";
import { IProduct } from "@/api/appApi/services/products/interfaces/product-interfaces";
const info = [
  "There is 1 manual locking system.",
  "There are 2 metal handles on the lid.",
  "Rubber-based gaskets are used on the lid seating surfaces.",
];
interface Props {
  product: IProduct;
}
const ProductDetails: React.FC<Props> = ({ product }) => {
  // const getProduct = useGetProduct(id);
  // const product = getProduct.data?.data;
  // if (getProduct.isLoading) return <Loading />;
  return (
    <div className="product-details">
      <div className="row m-0 common-details">
        <div className="col col-auto p-0 images">
          <ProductImagesSlider
            productImages={(product?.images ?? []).map(
              (url) => ImagBaseUrl + url
            )}
            id={product?.id ?? 0}
          />
        </div>
        <div className="col p-0 details">
          <div className="name">{product?.name}</div>
          <div className="stars">
            <ProductDatailsRating rate={3} />
          </div>
          <div className="extra-info">
            <div className="title">Color and logo options are available.</div>
            <div className="info">
              {info.map((item, key) => (
                <div key={key} className="one-info">
                  <SvgOneInfo /> {item}
                </div>
              ))}
            </div>
          </div>
          <div className="prcie-fav">
            <div className="price">
              <div className="current-price">{`${product?.price}$/pcs`}</div>
              <div className="price-without-discount">35.00$</div>
            </div>
            <div className="fav-stat">
              <SvgStats />
              <SvgLove />
            </div>
          </div>
        </div>
      </div>
      <Services />
      <CategoriesAndTypes />
      <Reviews />
      <RelatedProducts id={product.id} />
    </div>
  );
};

export default ProductDetails;
