const SvgStats = () => {
  return (
    <svg
      style={{ cursor: "pointer" }}
      xmlns="http://www.w3.org/2000/svg"
      width="64"
      height="64"
      viewBox="0 0 64 64"
      fill="none"
    >
      <g clipPath="url(#clip0_1_351)">
        <path
          d="M50.6667 8H13.3333C10.3867 8 8 10.3867 8 13.3333V50.6667C8 53.6133 10.3867 56 13.3333 56H50.6667C53.6133 56 56 53.6133 56 50.6667V13.3333C56 10.3867 53.6133 8 50.6667 8ZM50.6667 50.6667H13.3333V13.3333H50.6667V50.6667ZM24 45.3333H18.6667V32H24V45.3333ZM34.6667 45.3333H29.3333V18.6667H34.6667V45.3333ZM45.3333 45.3333H40V26.6667H45.3333V45.3333Z"
          fill="#A01246"
        />
      </g>
      <defs>
        <clipPath id="clip0_1_351">
          <rect width="64" height="64" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default SvgStats;
