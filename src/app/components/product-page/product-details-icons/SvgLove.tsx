const SvgLove = () => {
  return (
    <svg
      style={{ cursor: "pointer" }}
      xmlns="http://www.w3.org/2000/svg"
      width="64"
      height="64"
      viewBox="0 0 64 64"
      fill="none"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M32 16.0005C27.2016 10.4084 19.1834 8.68023 13.1713 13.8009C7.15915 18.9215 6.31272 27.4829 11.0341 33.5392C14.9596 38.5744 26.8395 49.1944 30.7331 52.6317C31.1685 53.0162 31.3864 53.2085 31.6405 53.284C31.8621 53.3498 32.1048 53.3498 32.3267 53.284C32.5808 53.2085 32.7984 53.0162 33.2341 52.6317C37.1277 49.1944 49.0075 38.5744 52.9331 33.5392C57.6544 27.4829 56.9112 18.8676 50.7957 13.8009C44.6803 8.73409 36.7984 10.4084 32 16.0005Z"
        stroke="#A01246"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default SvgLove;
