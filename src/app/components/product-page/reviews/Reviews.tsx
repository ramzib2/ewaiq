import ParagraphTitle from "../../common/ParagraphTitle";
import SvgOneInfo from "../product-details-icons/SvgOneInfo";
import ReviewSlider from "./ReviewSlider";

const Reviews = () => {
  return (
    <div className="product-details-review">
      <div className="review-header">
        <ParagraphTitle title="Reviews" noMargin />
        <div className="go-to-all-reviews">
          All Reviews <SvgOneInfo />
        </div>
      </div>

      <div className="mb-4" />
      <ReviewSlider />
      <div className="add-your-review">Add Your Review +</div>
    </div>
  );
};

export default Reviews;
