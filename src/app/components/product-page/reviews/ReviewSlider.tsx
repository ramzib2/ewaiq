import { WindowWidthAtom } from "@/hooks/useWinDim";
import KeenSlider from "@/app/components/common/keen-slider/KeenSlider";
import Review from "./review/Review";
import { useAtom } from "jotai";
interface props {}
const imgSize = 580;
const spacing = 64;

const ReviewSlider: React.FC<props> = ({}) => {
  const [w] = useAtom(WindowWidthAtom);
  const divWidth = w - 64;
  return (
    <div style={{ marginInlineEnd: -64 }}>
      <KeenSlider
        slidesPerView={divWidth / (imgSize + spacing)}
        spacing={spacing}
        autoplayDuration={0}
        autoplay={false}
        hidden={true}
      >
        {Array(20)
          .fill(0)
          .map((_, index) => (
            <Review key={index} />
          ))}
      </KeenSlider>
    </div>
  );
};

export default ReviewSlider;
