import SvgReview from "./SvgReview";
import SvgReviewStar from "./SvgReviewStar";

const Review = () => {
  return (
    <div className="review-card">
      <div className="review-header">
        <SvgReview />
      </div>
      <div className="review-stars">
        {Array(5)
          .fill(0)
          .map((_, key) => (
            <SvgReviewStar key={key} />
          ))}
      </div>
      <div className="review-text">
        This products is very useful, thanks EWA!
      </div>
      <div className="review-date">31/3/2024.</div>
    </div>
  );
};

export default Review;
