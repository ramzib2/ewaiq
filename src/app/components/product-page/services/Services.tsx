import Service from "./Service";
import SvgDelivery from "./services-icons/SvgDelivery";
import SvgHome from "./services-icons/SvgHome";
import SvgMoney from "./services-icons/SvgMoney";
import SvgQuality from "./services-icons/SvgQuality";

const services = [
  {
    icon: <SvgQuality />,
    content: "Quality Materials For Your Dream Project.",
  },
  {
    icon: <SvgHome />,
    content: "Build Your Home From Foundation To Finish.",
  },
  {
    icon: <SvgDelivery />,
    content: "Estimated delivery time between 15-30 day.",
  },
  {
    icon: <SvgMoney />,
    content: "Wide Variety Of Products To Fit Any Budget.",
  },
];
const Services = () => {
  return (
    <div className="row services">
      {services.map((service, index) => (
        <Service key={index} icon={service.icon} content={service.content} />
      ))}
    </div>
  );
};

export default Services;
