interface Props {
  icon: JSX.Element;
  content: string;
}
const Service: React.FC<Props> = ({ icon, content }) => {
  return (
    <div className="service">
      <div>{icon}</div>
      {content}
    </div>
  );
};

export default Service;
