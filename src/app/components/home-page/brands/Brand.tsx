import Image from "next/image";
interface Props {
  imgSrc: string;
}
const Brand: React.FC<Props> = ({ imgSrc }) => {
  return (
    <div>
      <Image
        src={imgSrc}
        alt=""
        width={190}
        height={92}
        style={{ mixBlendMode: "luminosity" }}
      />
    </div>
  );
};

export default Brand;
