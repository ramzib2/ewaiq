import Brand from "./Brand";
const imgsSrc = [
  "/images/brands/b1.png",
  "/images/brands/b2.png",
  "/images/brands/b3.png",
  "/images/brands/b4.png",
  "/images/brands/b5.png",
];
const Brands = () => {
  return (
    <div className="row">
      {imgsSrc.map((imgSrc, key) => (
        <div key={key} className="col">
          <Brand imgSrc={imgSrc} />
        </div>
      ))}
    </div>
  );
};

export default Brands;
