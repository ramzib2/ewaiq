import { getProductsSSR } from "@/api/appApi/services/products/products-ssr";
import Offers from "./Offers";

const OffersSC = async () => {
  const products = await getProductsSSR();
  return <Offers getProducts={products} />;
};

export default OffersSC;
