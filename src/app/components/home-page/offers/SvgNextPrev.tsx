interface Props {
  prev?: boolean;
  disabled?: boolean;
  onClick: () => void;
}
const SvgNextPrev: React.FC<Props> = ({ prev, disabled, onClick }) => {
  return (
    <svg
      onClick={disabled ? undefined : onClick}
      style={{
        transform: prev ? "scale(-1)" : "",
        cursor: disabled ? "not-allowed" : "pointer",
      }}
      width="49"
      height="48"
      viewBox="0 0 49 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="1.11424"
        y="0.685714"
        width="46.6286"
        height="46.6286"
        stroke={disabled ? "#8f8d8e" : "#231F20"}
        strokeWidth="1.37143"
      />
      <path
        d="M35.4 20.4369C38.1428 22.0205 38.1428 25.9795 35.4 27.5631L22.0285 35.2831C19.2857 36.8667 15.8571 34.8872 15.8571 31.72V16.28C15.8571 13.1128 19.2857 11.1333 22.0285 12.7169L35.4 20.4369Z"
        fill={disabled ? "#8f8d8e" : "#231F20"}
      />
    </svg>
  );
};

export default SvgNextPrev;
