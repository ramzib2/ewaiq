"use client";
import Product from "../../common/product/Product";
import ParagraphTitle from "../../common/ParagraphTitle";
import SvgNextPrev from "./SvgNextPrev";
import { useState } from "react";
import { IGetProducts } from "@/api/appApi/services/products/interfaces/product-interfaces";

const Offers: React.FC<{ getProducts: IGetProducts }> = ({ getProducts }) => {
  const [start, setStart] = useState(0);
  const products = getProducts?.data.data ?? [];
  const total = products?.length ?? 0;
  const step = 4;
  return (
    <div>
      <div className="offers-header">
        <ParagraphTitle title="Offers" noMargin />
        <div className="offers-btns">
          <div className="all-btn">ALL</div>
          <SvgNextPrev
            prev
            disabled={start === 0}
            onClick={() => setStart(start - step)}
          />
          <SvgNextPrev
            disabled={start >= total - step}
            onClick={() => setStart(start + step)}
          />
        </div>
      </div>
      <div className="d-flex" style={{ gap: 16 }}>
        {products?.slice(start, start + step).map((product, key) => (
          <Product key={key} product={product} />
        ))}
      </div>
    </div>
  );
};

export default Offers;
