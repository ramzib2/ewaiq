const SvgTest = () => {
  return (
    <svg>
      <text text-anchor="end" transform="translate(100,100) rotate(90)">
        ENGINEERING WORD ART
      </text>
    </svg>
  );
};

export default SvgTest;
