interface Props {
  height: number;
}
const BannerTextSide: React.FC<Props> = ({ height }) => {
  return (
    <div className="text-side" style={{ height: height }}>
      <div>
        <div className="vertical-text">
          <span>ENGINEERING WORD ART</span>
        </div>
        <div className="content">
          <div className="title1">
            <span className="first text-nowrap">Build </span>
            <span className="second">Smart</span>
          </div>
          <div className="title2 text-nowrap">
            <span className="first">Build </span>
            <span className="second">Fast</span>
          </div>
          <div className="text-description">
            Welcome to Engineering Word Art The best products at competitive
            prices.
          </div>
          <button className="banner-btn">Pay Now</button>
        </div>
      </div>
    </div>
  );
};

export default BannerTextSide;
