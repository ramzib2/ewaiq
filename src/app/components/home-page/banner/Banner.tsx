"use client";
import { WindowWidthAtom } from "@/hooks/useWinDim";
import BannerTextSide from "./BannerTextSide";
import { useAtom } from "jotai";
import Image from "next/image";
const h2w_img = 0.77;

const Banner = () => {
  const [w] = useAtom(WindowWidthAtom);
  const width = w - 400 < 400 ? w : w - 400;
  const height = h2w_img * width;
  return (
    <div className="home-banner">
      <BannerTextSide height={height} />
      {/* <div className="banner-img">
        <img src="./images/building.png" alt="" width="100%" />
      </div> */}
      <div className="banner-img">
        <Image
          src="/images/building.png"
          alt=""
          width={width}
          height={height}
          priority
        />
      </div>
    </div>
  );
};

export default Banner;
