import Advertisment from "./Advertisment";
import SvgDollar from "./adver-icons/SvgDollar";
import SvgEarphone from "./adver-icons/SvgEarphone";
import SvgPercent from "./adver-icons/SvgPercent";

const Advertisements = () => {
  return (
    <div className="row">
      <div className="col d-flex justify-content-center">
        <Advertisment Icon={<SvgPercent />} text="Special Offers" />
      </div>
      <div className="col col-auto border-style" />
      <div className="col d-flex justify-content-center">
        <Advertisment Icon={<SvgDollar />} text="Competitive Prices" />
      </div>
      <div className="col col-auto border-style" />
      <div className="col d-flex justify-content-center">
        <Advertisment Icon={<SvgEarphone />} text="7/24 Support" />
      </div>
    </div>
  );
};

export default Advertisements;
