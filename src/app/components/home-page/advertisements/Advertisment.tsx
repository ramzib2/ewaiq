import SvgPercent from "./adver-icons/SvgPercent";
interface Props {
  Icon: JSX.Element;
  text: string;
}
const Advertisment: React.FC<Props> = ({ Icon, text }) => {
  return (
    <div className="advertisement">
      {Icon}
      {text}
    </div>
  );
};

export default Advertisment;
