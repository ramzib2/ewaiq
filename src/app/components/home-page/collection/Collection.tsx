import ParagraphTitle from "../../common/ParagraphTitle";
import CollectionItem from "./CollectionItem";

const Collection = () => {
  return (
    <div>
      <ParagraphTitle title="Collection" noMargin />
      <div className="row" style={{ marginTop: 86, rowGap: 80 }}>
        <div className="col">
          <CollectionItem
            imgSrc="/images/collection/wing1.png"
            text="Construction"
            imgW={350}
            imgH={300}
          />
        </div>
        <div className="col">
          <CollectionItem
            imgSrc="/images/collection/wing2.png"
            text="Oil & Gas"
            imgW={239}
            imgH={300}
          />
        </div>
      </div>
      <div className="row" style={{ marginTop: 86, rowGap: 80 }}>
        <div className="col">
          <CollectionItem
            imgSrc="/images/collection/wing3.png"
            text="Mechanical"
            imgW={250}
            imgH={250}
          />
        </div>
        <div className="col">
          <CollectionItem
            imgSrc="/images/collection/wing4.png"
            text="Electrical"
            imgW={296}
            imgH={250}
          />
        </div>
      </div>
    </div>
  );
};

export default Collection;
