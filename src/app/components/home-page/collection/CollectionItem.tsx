import Image from "next/image";
interface Props {
  imgSrc: string;
  text: string;
  imgW: number;
  imgH: number;
}
const CollectionItem: React.FC<Props> = ({ imgSrc, text, imgW, imgH }) => {
  return (
    <div className="collection">
      <div className="text-box">{text}</div>
      <div style={{ paddingBottom: 32 }}>
        <Image src={imgSrc} alt="" width={imgW} height={imgH} />
      </div>
    </div>
  );
};

export default CollectionItem;
