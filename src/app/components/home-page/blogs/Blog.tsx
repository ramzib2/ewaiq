import Image from "next/image";
import SvgGoToBlog from "./SvgGoToBlog";
interface Props {
  imgSrc: string;
  name: string;
  date: string;
  description: string;
}
const Blog: React.FC<Props> = ({ imgSrc, name, date, description }) => {
  return (
    <div className="blog">
      <div className="blog-image">
        <Image src={imgSrc} alt="" width={576} height={360} />
      </div>
      <div className="blog-text">
        <div className="name-date">
          <div className="name">{name}</div>
          <div className="date">{date}</div>
        </div>
        <div className="description">{description}</div>
        <div className="text-center">
          <SvgGoToBlog />
        </div>
      </div>
    </div>
  );
};

export default Blog;
