import ParagraphTitle from "../../common/ParagraphTitle";
import Blog from "./Blog";
import SvgGoToAllBlogs from "./SvgGoToAllBlogs";

const Blogs = () => {
  return (
    <div>
      <ParagraphTitle title="Blogs" />
      <div className="row" style={{ gap: 32 }}>
        {Array(3)
          .fill(0)
          .map((_, key) => (
            <Blog
              key={key}
              imgSrc="/images/blogs/blog.png"
              name="Electronic"
              date="Jan 02, 2024 by Younus Ali"
              description="How to Match the Color of Your Handbag With an Outfit."
            />
          ))}
      </div>
      <div className="go-to-all-blogs">
        All Blogs
        <SvgGoToAllBlogs />
      </div>
    </div>
  );
};

export default Blogs;
