const SvgGoToAllBlogs = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="16"
      viewBox="0 0 13 16"
      fill="none"
    >
      <path
        d="M11.718 5.86819C13.359 6.81563 13.359 9.18425 11.718 10.1317L3.71797 14.7505C2.07694 15.6979 0.0256634 14.5136 0.0256634 12.6187L0.0256634 3.38114C0.0256634 1.48624 2.07695 0.301937 3.71797 1.24938L11.718 5.86819Z"
        fill="#231F20"
      />
    </svg>
  );
};

export default SvgGoToAllBlogs;
