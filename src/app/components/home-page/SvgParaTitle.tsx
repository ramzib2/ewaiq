const SvgParaTitle = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="19"
      viewBox="0 0 16 19"
      fill="none"
    >
      <path
        d="M14.5 6.90192C16.5 8.05662 16.5 10.9434 14.5 12.0981L4.75 17.7272C2.75 18.8819 0.25 17.4386 0.25 15.1292V3.87083C0.25 1.56143 2.75 0.118058 4.75 1.27276L14.5 6.90192Z"
        fill="#A01246"
      />
    </svg>
  );
};

export default SvgParaTitle;
