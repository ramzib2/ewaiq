import Image from "next/image";
import SvgLove from "./product-icons/SvgLove";
import SvgAddToCart from "./product-icons/SvgAddToCart";
import ProductStars from "./ProductStars";
import { IProduct } from "@/api/appApi/services/products/interfaces/product-interfaces";
import Link from "next/link";
import { ImagBaseUrl } from "@/app/utils/common-constants";
interface Props {
  product: IProduct;
}
const Product: React.FC<Props> = ({ product }) => {
  return (
    <div className="product-box">
      <div className="product-image">
        <Link href={`/products/${product.id}`}>
          <Image
            src={ImagBaseUrl + product.image}
            width={250}
            height={350}
            alt="product"
          />
        </Link>
      </div>
      <div className="box-content">
        <div className="name-star-fav">
          <div>
            <div className="name">{product?.name}</div>
            <ProductStars rate={3} />
          </div>
          <SvgLove />
        </div>
        <div className="price-add-to-cart">
          <div className="price">{product?.price}$</div>
          <button className="add-to-cart-btn">
            <SvgAddToCart />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Product;
