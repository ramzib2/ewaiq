import SvgEmptyStar from "./product-icons/SvgEmptyStar";
import SvgFullStar from "./product-icons/SvgFullStar";

interface Props {
  rate: number;
}
const ProductStars: React.FC<Props> = ({ rate }) => {
  return (
    <div>
      {Array(5)
        .fill(0)
        .map((_, index) =>
          5 - index > rate ? (
            <SvgEmptyStar key={index} />
          ) : (
            <SvgFullStar key={index} />
          )
        )}
    </div>
  );
};

export default ProductStars;
