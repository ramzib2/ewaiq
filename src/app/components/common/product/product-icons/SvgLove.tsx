const SvgLove = () => {
  return (
    <svg
      style={{ cursor: "pointer" }}
      xmlns="http://www.w3.org/2000/svg"
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24 12.0004C20.4012 7.80634 14.3875 6.5102 9.87846 10.3507C5.36936 14.1912 4.73454 20.6122 8.27556 25.1544C11.2197 28.9308 20.1296 36.8958 23.0498 39.4738C23.3764 39.7622 23.5398 39.9064 23.7304 39.963C23.8966 40.0124 24.0786 40.0124 24.245 39.963C24.4356 39.9064 24.5988 39.7622 24.9256 39.4738C27.8458 36.8958 36.7556 28.9308 39.6998 25.1544C43.2408 20.6122 42.6834 14.1508 38.0968 10.3507C33.5102 6.5506 27.5988 7.80634 24 12.0004Z"
        stroke="#A01246"
        strokeWidth="2.66667"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default SvgLove;
