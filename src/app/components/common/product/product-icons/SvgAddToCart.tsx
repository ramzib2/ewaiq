const SvgAddToCart = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="34"
      viewBox="0 0 32 34"
      fill="none"
    >
      <path
        d="M16 1.08286L16 32.9171"
        stroke="#E6E7E8"
        strokeWidth="1.98964"
        strokeLinecap="round"
      />
      <line
        x1="1.0777"
        y1="17"
        x2="30.9223"
        y2="17"
        stroke="#E6E7E8"
        strokeWidth="1.98964"
        strokeLinecap="round"
      />
    </svg>
  );
};

export default SvgAddToCart;
