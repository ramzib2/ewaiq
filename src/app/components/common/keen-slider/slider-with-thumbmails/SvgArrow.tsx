interface Props {
  right?: boolean;
  onClick: (e: any) => void;
  rtl: boolean;
  disabled: boolean;
}

const SvgArrow: React.FC<Props> = ({
  disabled,
  right = false,
  onClick,
  rtl,
}) => {
  return (
    <svg
      style={{
        position: "absolute",
        top: "50%",
        cursor: disabled ? "not-allowed" : "pointer",
        marginInlineStart:
          (right && !rtl) || (!right && rtl) ? `calc(100% - ${44}px)` : 10,
        transform: right
          ? "scaleX(-1) translate(0, -50%)"
          : "translate(0, -50%)",
        WebkitTransform: right
          ? "scaleX(-1) translate(0, -50%)"
          : "translate(0, -50%)",
      }}
      onClick={onClick}
      width="34"
      height="34"
      viewBox="0 0 34 34"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect x="0.5" y="0.5" width="33" height="33" rx="7.5" fill="white" />
      <path
        d="M19.9883 10.3941C19.8324 10.3987 19.6845 10.4639 19.5758 10.5757L13.5758 16.5757C13.4633 16.6882 13.4001 16.8408 13.4001 16.9999C13.4001 17.159 13.4633 17.3116 13.5758 17.4241L19.5758 23.4241C19.6311 23.4817 19.6973 23.5277 19.7706 23.5594C19.8439 23.591 19.9227 23.6078 20.0026 23.6086C20.0824 23.6094 20.1616 23.5943 20.2355 23.5641C20.3094 23.5339 20.3765 23.4893 20.433 23.4329C20.4894 23.3764 20.534 23.3093 20.5642 23.2354C20.5944 23.1615 20.6095 23.0823 20.6087 23.0024C20.6079 22.9226 20.5912 22.8438 20.5595 22.7705C20.5278 22.6972 20.4818 22.631 20.4242 22.5757L14.8485 16.9999L20.4242 11.4241C20.511 11.3399 20.5702 11.2314 20.5941 11.1128C20.6181 10.9943 20.6057 10.8713 20.5586 10.76C20.5115 10.6486 20.4318 10.5541 20.3301 10.4888C20.2283 10.4235 20.1092 10.3905 19.9883 10.3941Z"
        fill={`${disabled ? "gray" : "black"}`}
      />
      <rect x="0.5" y="0.5" width="33" height="33" rx="7.5" stroke="#F2F2F2" />
    </svg>
  );
};

export default SvgArrow;
