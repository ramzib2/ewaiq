import {
  useKeenSlider,
  KeenSliderPlugin,
  KeenSliderInstance,
} from "keen-slider/react";
import { MutableRefObject, useEffect, useState } from "react";
import SvgArrow from "./SvgArrow";
import Image from "next/image";
// import NextImage from "@/app/components/lib/NextImage";
// import { getcurrentLang } from "@/utils/functions/getcurrentLang";

function ThumbnailPlugin(
  mainRef: MutableRefObject<KeenSliderInstance | null>
): KeenSliderPlugin {
  return (slider) => {
    function removeActive() {
      slider.slides?.forEach((slide) => {
        slide.classList.remove("active");
      });
    }
    function addActive(idx: number) {
      slider.slides[idx]?.classList?.add("active");
    }

    function addClickEvents() {
      slider.slides.forEach((slide, idx) => {
        slide?.addEventListener("click", () => {
          if (mainRef.current) mainRef.current.moveToIdx(idx);
        });
      });
    }

    slider.on("created", () => {
      if (!mainRef.current) return;
      addActive(slider.track.details.rel);
      addClickEvents();
      mainRef.current.on("animationStarted", (main) => {
        removeActive();
        const next = main.animator.targetIdx || 0;
        addActive(main.track.absToRel(next));
        slider.moveToIdx(next);
      });
    });
  };
}
interface Props {
  children: JSX.Element[];
  thumbnails: string[];
  id: number;
  thumbPerview: number;
  size?: number;
  isSamll?: boolean;
  sliderWidth: number;
}
const isRtl = false;
const ThumbMailSlider: React.FC<Props> = ({
  children,
  thumbnails,
  id,
  thumbPerview,
  size = 137,
  isSamll = false,
  sliderWidth,
}) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
    initial: 0,
    rtl: isRtl,
    renderMode: "performance",
    slideChanged(slider) {
      setCurrentSlide(slider.track.details.rel);
    },
  });
  const [thumbnailRef, ins] = useKeenSlider<HTMLDivElement>(
    {
      initial: 0,
      rtl: isRtl,
      renderMode: "performance",
      slides: {
        perView: thumbPerview === 0 ? 1 : thumbPerview,
        spacing: 32,
      },
    },
    [ThumbnailPlugin(instanceRef)]
  );
  useEffect(() => {
    instanceRef.current?.update();
    instanceRef && instanceRef.current?.moveToIdx(0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);
  useEffect(() => {
    ins.current?.update();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [children[0], id]);
  return (
    <>
      <div style={{ position: "relative" }}>
        <div ref={sliderRef} className="keen-slider" style={{ minHeight: 200 }}>
          {children?.map((child, key) => (
            <div key={key} className="keen-slider__slide">
              {child}
            </div>
          ))}
        </div>
        {instanceRef.current && thumbnails?.length > 1 && (
          <>
            <SvgArrow
              rtl={isRtl}
              onClick={(e: any) =>
                e.stopPropagation() || instanceRef.current?.prev()
              }
              disabled={currentSlide === 0}
            />

            <SvgArrow
              right
              rtl={isRtl}
              onClick={(e: any) =>
                e.stopPropagation() || instanceRef.current?.next()
              }
              disabled={
                currentSlide ===
                instanceRef.current?.track?.details?.slides?.length - 1
              }
            />
          </>
        )}
      </div>
      {instanceRef.current && thumbnails?.length > 1 && (
        <div
          ref={thumbnailRef}
          className="keen-slider slider-thumbmail"
          style={{ marginTop: 24, maxWidth: sliderWidth }}
        >
          {thumbnails?.map((img, i) => (
            <div
              key={i}
              className={`thumbmail keen-slider__slide`}
              style={{
                width: size,
                height: size,
                borderRadius: isSamll ? 8 : 16,
              }}
            >
              <Image
                alt="product"
                src={img}
                width={size}
                height={size}
                objectFit="contain"
              />
            </div>
          ))}
        </div>
      )}
    </>
  );
};

export default ThumbMailSlider;
