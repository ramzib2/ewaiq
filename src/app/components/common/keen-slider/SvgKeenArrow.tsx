interface Props {
  left?: boolean;
  onClick: () => void;
  disabled: boolean;
}
const SvgKeenArrow: React.FC<Props> = ({ onClick, left = false, disabled }) => {
  return (
    <svg
      onClick={(e) => {
        e.stopPropagation();
        if (disabled) return;
        onClick();
      }}
      className={`slider-arrow ${left ? "arrow-left" : "arrow-right"} ${
        disabled ? "disabled" : ""
      }`}
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16 0.5C24.5604 0.5 31.5 7.43958 31.5 16C31.5 24.5604 24.5604 31.5 16 31.5C7.43959 31.5 0.5 24.5604 0.5 16C0.5 7.43959 7.43959 0.5 16 0.5Z"
        fill="white"
        className="stroke-gray4"
      />
      <path
        d="M12.0002 8.72796C12.0046 8.91553 12.083 9.09375 12.2183 9.22379L18.8266 15.8321L12.2183 22.4405C12.1501 22.5067 12.096 22.586 12.059 22.6735C12.022 22.7611 12.003 22.8551 12.003 22.9502C12.003 23.0916 12.0452 23.2299 12.1243 23.3472C12.2033 23.4646 12.3155 23.5557 12.4465 23.6089C12.5776 23.6621 12.7216 23.6751 12.86 23.6461C12.9985 23.617 13.1251 23.5474 13.2238 23.446L20.3349 16.3349C20.4682 16.2015 20.5431 16.0207 20.5431 15.8321C20.5431 15.6436 20.4682 15.4627 20.3349 15.3293L13.2238 8.21824C13.1245 8.11496 12.9963 8.04409 12.856 8.01491C12.7158 7.98573 12.57 7.99959 12.4377 8.05468C12.3054 8.10978 12.1929 8.20354 12.1148 8.32367C12.0368 8.44381 11.9968 8.58473 12.0002 8.72796Z"
        className={`${disabled ? "fill-gray4" : "fill-secondary"}`}
      />
    </svg>
  );
};

export default SvgKeenArrow;
