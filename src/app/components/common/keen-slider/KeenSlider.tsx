"use client";
import { CSSProperties, useEffect, useState } from "react";
import { useKeenSlider } from "keen-slider/react";
import SvgKeenArrow from "./SvgKeenArrow";

const style = (rtl: boolean, isEnd: boolean): CSSProperties => {
  const width = 89;
  return {
    position: "absolute",
    width: width,
    height: "100%",
    top: 0,
    background:
      "linear-gradient(270deg, #FAFAFA 0%, rgba(250, 250, 250, 0) 100%)",
    marginInlineStart: isEnd ? `calc(100% - ${width}px)` : 0,
    transform: (isEnd && !rtl) || (!isEnd && rtl) ? "" : "scaleX(-1)",
    WebkitTransform: (isEnd && !rtl) || (!isEnd && rtl) ? "" : "scaleX(-1)",
  };
};

interface Props {
  slidesPerView: number | "auto";
  spacing: number;
  autoplayDuration: number;
  autoplay: boolean;
  hidden: boolean;
  children: JSX.Element[];
  setIsLastSlide?: (isLastSlide: boolean) => void;
}
const isRtl = false;
const KeenSlider: React.FC<Props> = ({
  slidesPerView,
  spacing,
  autoplay = true,
  hidden = false,
  autoplayDuration = 5000,
  children,
  setIsLastSlide,
}) => {
  const [loaded, setLoaded] = useState(false);
  const [currentSlide, setCurrentSlide] = useState(0);
  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>(
    {
      rtl: isRtl,
      loop: autoplay,
      renderMode: "performance",
      drag: true,
      slides: {
        perView: slidesPerView,
        spacing: spacing,
      },
      slideChanged(slider) {
        setCurrentSlide(slider.track.details.rel);
        if (
          setIsLastSlide &&
          slider.track.details.rel === slider.track.details.maxIdx
        ) {
          setIsLastSlide(true);
        }
      },
      created() {
        setLoaded(true);
      },
    },
    [
      (slider) => {
        if (!autoplay) return;
        let timeout: ReturnType<typeof setTimeout>;
        let mouseOver = false;
        function clearNextTimeout() {
          clearTimeout(timeout);
        }
        function nextTimeout() {
          clearTimeout(timeout);
          if (mouseOver) return;
          timeout = setTimeout(() => {
            slider.next();
          }, autoplayDuration);
        }
        slider.on("created", () => {
          slider?.container?.addEventListener("mouseover", () => {
            mouseOver = true;
            clearNextTimeout();
          });
          slider?.container?.addEventListener("mouseout", () => {
            mouseOver = false;
            nextTimeout();
          });
          nextTimeout();
        });
        slider.on("dragStarted", clearNextTimeout);
        slider.on("animationEnded", nextTimeout);
        slider.on("updated", nextTimeout);
      },
    ]
  );
  useEffect(() => {
    instanceRef && instanceRef.current?.moveToIdx(0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loaded]);
  useEffect(() => {
    instanceRef.current?.update();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [children]);
  const disableLeft = currentSlide === 0;
  const disableRight =
    currentSlide >=
    (instanceRef.current?.track.details?.slides?.length ?? 0) -
      Math.floor(+slidesPerView);
  return (
    <div className="keen-Slider-container">
      <div ref={sliderRef} className="keen-slider">
        {children.map((child, key) => (
          <div
            key={key}
            className="keen-slider__slide"
            style={{
              overflow: "visible",
              boxSizing: "initial",
              padding: "25px 0px",
            }}
          >
            {child}
          </div>
        ))}
      </div>
      {!hidden && loaded && instanceRef.current && (
        <>
          {currentSlide !==
            instanceRef.current.track.details?.slides?.length -
              Math.floor(+slidesPerView) && <div style={style(isRtl, true)} />}
          {currentSlide !== 0 && <div style={style(isRtl, false)} />}

          {instanceRef.current.track.details?.slides?.length >
            +slidesPerView && (
            <>
              <SvgKeenArrow
                disabled={isRtl ? disableLeft : disableRight}
                onClick={() => {
                  isRtl
                    ? instanceRef.current?.prev()
                    : instanceRef.current?.next();
                }}
              />
              <SvgKeenArrow
                left
                disabled={isRtl ? disableRight : disableLeft}
                onClick={() => {
                  isRtl
                    ? instanceRef.current?.next()
                    : instanceRef.current?.prev();
                }}
              />
            </>
          )}
        </>
      )}
    </div>
  );
};

export default KeenSlider;
