import SvgParaTitle from "../home-page/SvgParaTitle";

interface Props {
  title: string;
  noMargin?: boolean;
}
const ParagraphTitle: React.FC<Props> = ({ title, noMargin }) => {
  return (
    <div className={`para-title ${!noMargin ? "with-margin" : ""}`}>
      <SvgParaTitle />
      {title}
    </div>
  );
};

export default ParagraphTitle;
