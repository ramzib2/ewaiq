const Loading = () => {
  return (
    <div className="d-flex justify-content-center align-items-center my-5">
      <div
        className="spinner-border text-secondary"
        style={{ width: 100, height: 100 }}
        role="status"
      >
        <span className="visually-hidden">Loading...</span>
      </div>
    </div>
  );
};

export default Loading;
