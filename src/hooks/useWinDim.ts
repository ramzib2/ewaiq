"use client";
import { atom, useAtom } from "jotai";
// import { atom, useAtom } from "@/utils/functions/client-packages";
import { useState, useEffect, useCallback } from "react";
const debounce = (fn: Function, ms: number) => {
  let timer: any;
  return function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
      timer = null;
      //@ts-ignore
      fn.apply(this, arguments);
    }, ms);
  };
};
export const WindowWidthAtom = atom(0);

export const useWindowDimension = () => {
  const [, setDimension] = useAtom(WindowWidthAtom);
  useEffect(() => {
    const debouncedResizeHandler = debounce(() => {
      setDimension(window?.innerWidth);
    }, 100);
    window?.addEventListener("resize", debouncedResizeHandler);
    setDimension(window?.innerWidth);
    return () => {
      window?.removeEventListener("resize", debouncedResizeHandler);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
